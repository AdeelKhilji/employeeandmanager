/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 * Class PayrollSimulation
 * @author Adeel Khilji
 */
public class PayrollSimulation 
{
    /**
     * main method
     * @param args String 
     */
    public static void main(String[] args)
    {
        Employee employee = new Employee("Jon Doe", 16.50, 44);//Employee object
        Manager manager = new Manager("Mary Jane", 25.45, 30, 200.50);//Manager object
        /**
         * Local variables for string formats type String
         */
        String employeeFormat = " NAME: " + employee.getName() + " HOURS WORKED: " + employee.getNumOfHours() + " HOURLY WAGE: " + employee.getHourlyWage() + " TOTAL PAY: " + employee.calculatePay();
        /*
         * Local variables for string formats type String
         */
        String managerFormat = " NAME: " + manager.getName() + " HOURS WORKED: " + manager.getNumOfHours() + " HOURLY WAGE: " + manager.getHourlyWage() + " BONUS: " + manager.getBonus() + " TOTAL PAY: " + manager.calculatePay();
        System.out.println("EMPLOYEE " + employeeFormat.toString());//Displaying employee information
        System.out.println("MANAGER " + managerFormat.toString());//Displaying manager information
    }
}
