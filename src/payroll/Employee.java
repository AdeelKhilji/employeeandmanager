    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 * Class Employee
 * @author Adeel Khilji
 */
public class Employee 
{
    private String name;//Instance variable name
    private double hourlyWage;//Instance variable hourlyWage
    private int numOfHours;//Instance variable numOfHours
    
    /**
     * Constructor with three parameters
     * @param name String
     * @param hourlyWage double
     * @param numOfHours int
     */
    protected Employee(String name, double hourlyWage, int numOfHours)
    {
        this.name = name;
        this.hourlyWage = hourlyWage;
        this.numOfHours = numOfHours;
    }
    /**
     * getName() - getter method
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
    /**
     * getHourlyWage() - getter method
     * @return double
     */
    public double getHourlyWage()
    {
        return this.hourlyWage;
    }
    
    /**
     * getNumOfHours() - getter method
     * @return int
     */
    public int getNumOfHours()
    {
        return this.numOfHours;
    }
    /**
     * calculatePay() - calculate method
     * @return double
     */
    public double calculatePay()
    {
        return this.hourlyWage * this.numOfHours;
    }
}
